#Deal Load test main configuration
>VehicleName=toyota

>EmailHostName=@gmail.com

>DriverUserName=LoadTestUser

>numberOfOrders=100

>numberofOrdersPerVehicle=5


#Deal Load Test now been set to Paralel Load Test Scenario
Now it's been set to do a parallel load test.

#Instalation and run Deal Load Test
To install the application

clean install -DskipTests

To run the program

exec:java

#Setting for Driver username in driver creation
In order to avoid existing username error when creation driver, the username will be merged with UUID.

example if DriverUserName is set as "LoadTestUser"

then user name will looked like : LoadTestUserb725b5db-f1d5-47c8-b155-e054a0747357

and with email host name configuration 'EmailHostName'

then email name will be : LoadTestUserb725b5db-f1d5-47c8-b155-e054a0747357@gmail.com


#Setting number of orders and number orders per vehicle
Every created Driver will be set as a thread that will independently process order.

this is for feature that order will be assigned in round robin style in every driver thread.

so number of driver will be the divided value of numberOfOrders and numberOfOrdersPerVehicle.

example : if number of orders (numberOfOrders) is set as 100 and number of orders per vehicle (numberofOrdersPerVehicle) is set as 5,

then there will be 20 drivers assigned with 5 orders for each.


#Setting for vehicle name in vehicle creation

Vehicle name simply will follow 'VehicleName' attribute from dealrest.properties

#Behavior of Deal Parallel Load Test
Every time the system is run, it will directly access the attributes of dealrest.properties to create drivers and vehicles.

Every created driver will be automatically assigned with one vehicle automatically.

Every created driver will be stand as one thread to process each of it's assigned orders.


Old Readme : https://docs.google.com/document/d/1_4PQEb-24WOjA9LZuHFQ5uT0aGilR5b-j43c68hs9nQ