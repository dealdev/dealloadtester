package com.dealservices.rest.client.restattributes;

import com.dealservices.rest.client.RestClient;

/**
 * Created by deal-04 on 13/08/15.
 */
public class PickUpTrajectoryStarterLocalhostTest {
    public static void main(String [] args){
        String baseUrl = "http://localhost:9090";

        RestClient restClient= new RestClient();

        /*REST ACTION - LOGIN*/
        Login login = new Login();
        login.setMethod("post");
        login.setBaseUrl(baseUrl);
        login.setEndPointUrl("/auth/loginCredentials");
        login.setUserName("dsubco@gmail.com");
        login.setPassWord("dealdeal");


        restClient.doRest(login);
        System.out.println("[LOG] Login Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString()+"\n");
        System.out.println("accessTokenUuids = "+login.getResponseJson().getJSONArray("accessTokenUuids").getString(0));

        /*REST ACTION - CREATE ORDER*/

        Order order = new Order();
        order.setMethod("post");
        order.setBaseUrl(baseUrl);
        order.setPayloadBodyFromFileUrl("/home/deal-04/Documents/jsonfile/order.json");
        order.setCompanyUUid(login.getResponseJson().getString("companyUuid"));
        order.setAuthUUID(restClient.getAuthUUID()); //optional, because auth tokens/uuid will be saved in Restclient object

        restClient.doRest(order);
        System.out.println("[LOG] Create Order Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString()+"\n");

        /*REST ACTION - ASSIGN ORDER TO VEHICLE*/

        AssignOrderVehicle assignOrderVehicle=new AssignOrderVehicle();
        assignOrderVehicle.setMethod("post");
        assignOrderVehicle.setBaseUrl(baseUrl);
        assignOrderVehicle.setVehicleUUID("00e74660-47aa-4d0e-a6c4-36693ef88eb7");
        assignOrderVehicle.setCompanyUUID(login.getResponseJson().getString("companyUuid"));
        assignOrderVehicle.setOrderId(order.getResponseJson().getString("_id"));

        restClient.doRest(assignOrderVehicle);
        System.out.println("[LOG] ASSIGN Order to Vehicle Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - Get Order Trajectories*/

        GetOrderTrajectories getOrderTrajectories=new GetOrderTrajectories();
        getOrderTrajectories.setMethod("get");
        getOrderTrajectories.setBaseUrl(baseUrl);
        getOrderTrajectories.setCompanyUUID(login.getResponseJson().getString("companyUuid"));
        getOrderTrajectories.setOrderId(order.getResponseJson().getString("_id"));

        restClient.doRest(getOrderTrajectories);
        System.out.println("[LOG] getOrderTrajectories Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonArray().getJSONObject(0).toString()+ "\n \n"+ restClient.getRestAction().getResponseJsonArray().getJSONObject(1).toString());

        /*REST ACTION - Trajectories Starter*/

        PickUpTrajectoryStarter pickUpTrajectoryStarter = new PickUpTrajectoryStarter();
        pickUpTrajectoryStarter.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(0));
        pickUpTrajectoryStarter.setBaseUrl(baseUrl);
        pickUpTrajectoryStarter.setMethod("put");
        pickUpTrajectoryStarter.setAltitude(114.0);
        pickUpTrajectoryStarter.setSpeed(17);

        restClient.doRest(pickUpTrajectoryStarter);
        System.out.println("[LOG] Starter Trajectory Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");
    }
}
