package com.dealservices.rest.client.restattributes;

import com.dealservices.rest.client.RestClient;
import org.json.JSONObject;

/**
 * Created by deal-04 on 07/08/15.
 */
public class RestClientTest {

    public static void main (String [] args){
        Login login = new Login();
        login.setBaseUrl("https://test1.dealservices.nl:9091");
        login.setEndPointUrl("/auth/loginCredentials");
        login.setUserName("dsubco@gmail.com");
        login.setPassWord("dealdeal");

        RestClient restClient= new RestClient();
        restClient.doRest(login);
        JSONObject accessTokenUuids=login.getResponseJson();
        System.out.println("accessTokenUuids = "+accessTokenUuids.getJSONArray("accessTokenUuids").getString(0));
    }
}
