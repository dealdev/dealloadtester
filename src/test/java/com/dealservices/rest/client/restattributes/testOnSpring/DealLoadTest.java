package com.dealservices.rest.client.restattributes.testOnSpring;

import com.dealservices.rest.client.RestClient;
import com.dealservices.rest.client.restattributes.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * Created by deal-04 on 19/08/15.
 */


public class DealLoadTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("DealRestActionBeans.xml");

        RestClient restClient= (RestClient) context.getBean("restClient");
        Login login = (Login) context.getBean("login");
        Order order = (Order) context.getBean("order");
        AssignOrderVehicle assignOrderVehicle= (AssignOrderVehicle) context.getBean("assignOrderVehicle");
        GetOrderTrajectories getOrderTrajectories= (GetOrderTrajectories) context.getBean("getOrderTrajectories");
        PickUpTrajectoryStarter pickUpTrajectoryStarter= (PickUpTrajectoryStarter) context.getBean("pickUpTrajectoryStarter");
        PickUpTrajectoryFinisher pickUpTrajectoryFinisher= (PickUpTrajectoryFinisher) context.getBean("pickUpTrajectoryFinisher");
        DeliveryTrajectoryStarter deliveryTrajectoryStarter= (DeliveryTrajectoryStarter) context.getBean("deliveryTrajectoryStarter");
        DeliveryTrajectoryFinisher deliveryTrajectoryFinisher= (DeliveryTrajectoryFinisher) context.getBean("deliveryTrajectoryFinisher");


        restClient.doRest(login);
        System.out.println("[LOG] Login Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");
        System.out.println("accessTokenUuids = " + login.getResponseJson().getJSONArray("accessTokenUuids").getString(0));

        /*REST ACTION - CREATE ORDER*/
        order.setCompanyUUid(login.getResponseJson().getString("companyUuid"));
        order.setAuthUUID(restClient.getAuthUUID());
        restClient.doRest(order);
        System.out.println("[LOG] Create Order Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - ASSIGN ORDER TO VEHICLE*/
        assignOrderVehicle.setCompanyUUID(login.getResponseJson().getString("companyUuid"));
        assignOrderVehicle.setOrderId(order.getResponseJson().getString("_id"));
        restClient.doRest(assignOrderVehicle);
        System.out.println("[LOG] ASSIGN Order to Vehicle Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - Get Order Trajectories*/
        getOrderTrajectories.setCompanyUUID(login.getResponseJson().getString("companyUuid"));
        getOrderTrajectories.setOrderId(order.getResponseJson().getString("_id"));
        restClient.doRest(getOrderTrajectories);
        System.out.println("[LOG] getOrderTrajectories Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonArray().getJSONObject(0).toString() + "\n \n" + restClient.getRestAction().getResponseJsonArray().getJSONObject(1).toString());

        /*REST ACTION - Trajectories Starter*/
        pickUpTrajectoryStarter.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(0));
        restClient.doRest(pickUpTrajectoryStarter);
        System.out.println("[LOG] Starter Trajectory Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - Trajectories Finisher*/
        pickUpTrajectoryFinisher.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(0));
        restClient.doRest(pickUpTrajectoryFinisher);
        System.out.println("[LOG] Finisher Trajectory Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - deliveryTrajectoryStarter*/

        deliveryTrajectoryStarter.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(1));
        restClient.doRest(deliveryTrajectoryStarter);
        System.out.println("[LOG] deliveryTrajectoryStarter Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");


        /*REST ACTION - deliveryTrajectoryFinisher*/
        deliveryTrajectoryFinisher.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(1));
        restClient.doRest(deliveryTrajectoryFinisher);
        System.out.println("[LOG] deliveryTrajectoryFinisher Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");

    }
}