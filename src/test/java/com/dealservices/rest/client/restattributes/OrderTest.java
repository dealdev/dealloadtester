package com.dealservices.rest.client.restattributes;

import com.dealservices.rest.client.RestClient;
import org.json.JSONObject;

/**
 * Created by deal-04 on 09/08/15.
 */
public class OrderTest {
    public static void main(String [] args){

        /*REST ACTION - LOGIN*/
        Login login = new Login();
        login.setMethod("post");
        login.setBaseUrl("https://test1.dealservices.nl:9091");
        login.setEndPointUrl("/auth/loginCredentials");
        login.setUserName("dsubco@gmail.com");
        login.setPassWord("dealdeal");

        RestClient restClient= new RestClient();
        restClient.doRest(login);
        JSONObject jSONObject=login.getResponseJson();

        System.out.println("Login Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString());

        System.out.println("accessTokenUuids = "+jSONObject.getJSONArray("accessTokenUuids").getString(0));

        /*REST ACTION - CREATE ORDER*/
        Order order = new Order();
        order.setMethod("post");
        order.setBaseUrl("https://test1.dealservices.nl:9091");
        order.setPayloadBodyFromFileUrl("/home/deal-04/Documents/jsonfile/order.json");
        order.setCompanyUUid(jSONObject.getString("companyUuid"));
        order.setAuthUUID(restClient.getAuthUUID()); //optional, because auth tokens/uuid will be saved in Restclient object

        restClient.doRest(order);

        System.out.println("Create Order Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString());
    }
}
