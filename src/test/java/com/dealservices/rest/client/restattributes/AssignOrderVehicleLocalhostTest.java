package com.dealservices.rest.client.restattributes;

import com.dealservices.rest.client.RestClient;
import org.json.JSONObject;

/**
 * Created by deal-04 on 11/08/15.
 */
public class AssignOrderVehicleLocalhostTest {

    public static void main(String [] args){
        String baseUrl ="http://localhost:9090";
        RestClient restClient= new RestClient();

        /*REST ACTION - LOGIN*/
        Login login = new Login();
        login.setMethod("post");
        login.setBaseUrl(baseUrl);
        login.setEndPointUrl("/auth/loginCredentials");
        login.setUserName("dsubco@gmail.com");
        login.setPassWord("dealdeal");


        restClient.doRest(login);
        JSONObject jSONObject=login.getResponseJson();

        System.out.println("Login Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString()+"\n");

        System.out.println("accessTokenUuids = "+jSONObject.getJSONArray("accessTokenUuids").getString(0));

        /*REST ACTION - CREATE ORDER*/

        Order order = new Order();
        order.setMethod("post");
        order.setBaseUrl(baseUrl);
        order.setPayloadBodyFromFileUrl("/home/deal-04/Documents/jsonfile/order.json");
        order.setCompanyUUid(jSONObject.getString("companyUuid"));
        order.setAuthUUID(restClient.getAuthUUID()); //optional, because auth tokens/uuid will be saved in Restclient object

        restClient.doRest(order);

        System.out.println("Create Order Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString()+"\n");

        /*REST ACTION - ASSIGN ORDER TO VEHICLE*/

        AssignOrderVehicle assignOrderVehicle=new AssignOrderVehicle();
        assignOrderVehicle.setMethod("post");
        assignOrderVehicle.setBaseUrl(baseUrl);
        assignOrderVehicle.setVehicleUUID("00e74660-47aa-4d0e-a6c4-36693ef88eb7");
        assignOrderVehicle.setCompanyUUID(jSONObject.getString("companyUuid"));
        assignOrderVehicle.setOrderId(order.getResponseJson().getString("_id"));

        restClient.doRest(assignOrderVehicle);
        System.out.println("ASSIGN Order to Vehicle Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() +"\n");

    }
}
