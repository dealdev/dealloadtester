package com.dealservices.rest.client.restattributes;

import com.dealservices.rest.client.RestClient;
import org.json.JSONObject;

/**
 * Created by deal-04 on 14/08/15.
 */
public class UploadLocationsLocalhostTest {

    public static void main(String [] args) {
        String baseUrl = "http://localhost:9090";

        RestClient restClient = new RestClient();

        /*REST ACTION - LOGIN*/
        Login login = new Login();
        login.setMethod("post");
        login.setBaseUrl(baseUrl);
        login.setEndPointUrl("/auth/loginCredentials");
        login.setUserName("dsubco@gmail.com");
        login.setPassWord("dealdeal");

        restClient.doRest(login);
        JSONObject jSONObject=login.getResponseJson();

        System.out.println("Login Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");

        System.out.println("accessTokenUuids = " + jSONObject.getJSONArray("accessTokenUuids").getString(0)+"\n");

        /*REST ACTION - UPLOAD LOCATIONS*/
        UploadLocations uploadLocations= new UploadLocations();
        uploadLocations.setBaseUrl(baseUrl);
        uploadLocations.setCompanyUUID("b725b5db-f1d5-47c8-b155-e054a0747357");
        uploadLocations.setVehicleUUID("00e74660-47aa-4d0e-a6c4-36693ef88eb7");
        uploadLocations.setPayloadBodyFromFileUrl("/home/deal-04/Documents/jsonfile/UploadLocations.json");
        uploadLocations.setMethod("put");

        restClient.doRest(uploadLocations);
//        System.out.println(restClient.getRestAction().getResponseJsonArray().getJSONObject(0).toString()+ "\n \n"+ restClient.getRestAction().getResponseJsonArray().getJSONObject(1).toString());

        System.out.println("UPLOAD LOCATIONS - Output from Server .... \n");
        System.out.println(restClient.getRestAction().getResponseJsonString() + "\n");
    }

}
