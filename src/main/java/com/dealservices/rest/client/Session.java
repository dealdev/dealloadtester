package com.dealservices.rest.client;

/**
 * Created by deal-04 on 19/08/15.
 */
public class Session {
    private long sequenceDelay;
    private String authUUID;
    private String JSessionId;
    private int orderLimit;

    public int getOrderLimit() {
        return orderLimit;
    }

    public void setOrderLimit(int orderLimit) {
        this.orderLimit = orderLimit;
    }

    public String getAuthUUID() {
        return authUUID;
    }

    public void setAuthUUID(String authUUID) {
        this.authUUID = authUUID;
    }

    public String getJSessionId() {
        return JSessionId;
    }

    public void setJSessionId(String JSessionId) {
        this.JSessionId = JSessionId;
    }

    public long getSequenceDelay() {
        return sequenceDelay;
    }

    public void setSequenceDelay(long sequenceDelay) {
        this.sequenceDelay = sequenceDelay;
    }

}
