package com.dealservices.rest.client;

import com.dealservices.rest.client.restattributes.Login;

/**
 * Created by deal-04 on 12/09/15.
 */
public class AdministratorSession extends Login {
    RestClient restClient=new RestClient();

    public void init(){
        if(this.getUserName()==null||this.getPassWord()==null||this.getHttpUrl()==null){
            throw new NullPointerException();
        }else{
            restClient.doRest(this);
        }
    }

}
