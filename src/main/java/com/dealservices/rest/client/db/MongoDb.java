package com.dealservices.rest.client.db;

import com.dealservices.rest.client.common.MgmtProperties;
import com.dealservices.rest.client.common.UserAuth;
import com.mongodb.WriteResult;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @author Vishal
 * @since June 2015
 * Mongo db class to handle all the mongo db
 * needs of management application.
 */
public class MongoDb {

    private static String AUTH_COLLECTION = "userauth" ;
    
    /**
     * Insert the authentication information in db. 
     * @param userAuth
     */
    public void insertUserAuth(UserAuth userAuth) {
        Jongo jongo = MgmtProperties.getInstance().getJongo() ;
        MongoCollection collection = jongo.getCollection(AUTH_COLLECTION); 
        if(collection!=null) {
            WriteResult res = collection.insert(userAuth) ;
            if(res.getError()!=null) {
                throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).
                                                  entity(res.getError()).type(MediaType.TEXT_PLAIN).build()) ;

            }
        }

    }

}
