package com.dealservices.rest.client;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by deal-04 on 12/09/15.
 */
public class Planner {
    Administrator administrator;

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public static void main(String [] args){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("DealRestActionBeans.xml");
        Composer composer=(Composer) context.getBean("composer");

        int numberOfOrder=composer.getNumberOfOrder();
        int numberofOrderPerVehicle=composer.getNumberOfOrderPerVehicle();

        while(numberOfOrder>0){
            Administrator driver=composer.createAdministrator();
            if(numberOfOrder - numberofOrderPerVehicle>0){
                driver.setOrderperDriver(numberofOrderPerVehicle);
                numberOfOrder=numberOfOrder-numberofOrderPerVehicle;
//                composer.setNumberOfOrder(composer.getNumberOfOrder() - composer.getNumberOfOrderPerVehicle());
            }else{
                driver.setOrderperDriver(composer.getNumberOfOrder());
                numberOfOrder=numberOfOrder-numberofOrderPerVehicle;
            }
            new Thread(driver).start();
        }
    }
}
