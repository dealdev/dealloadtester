package com.dealservices.rest.client.restattributes;

/**
 * Created by deal-04 on 07/08/15.
 */
public class Login extends RestAction{
    protected String JSESSIONID;
    protected String userName;
    protected String passWord;
    protected String companyUUID;

    public String getJSESSIONID() {
        return JSESSIONID;
    }

    public void setJSESSIONID(String JSESSIONID) {
        this.JSESSIONID = JSESSIONID;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompanyUUID() {
        return companyUUID;
    }

    public void setCompanyUUID(String companyUUID) {
        this.companyUUID = companyUUID;
    }

    @Override
    public String getEndPointUrl() {
        return this.endPointUrl;
    }

    @Override
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    @Override
    public String getRestInput() {
        if(this.userName != null && this.passWord!=null){
            return "{\"username\": \"" + this.userName + "\",\"password\": \"" + this.passWord + "\"}";
        }else {
            return this.restInput;
        }

    }

    @Override
    public void setRestInput(String restInput) {
        this.restInput=restInput;
    }


}
