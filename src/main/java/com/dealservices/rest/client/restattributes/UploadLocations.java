package com.dealservices.rest.client.restattributes;

/**
 * Created by deal-04 on 14/08/15.
 */
public class UploadLocations extends RestAction{
    private String companyUUID;
    private String VehicleUUID;

    public String getCompanyUUID() {
        return companyUUID;
    }

    public void setCompanyUUID(String companyUUID) {
        this.companyUUID = companyUUID;
    }

    public String getVehicleUUID() {
        return VehicleUUID;
    }

    public void setVehicleUUID(String vehicleUUID) {
        VehicleUUID = vehicleUUID;
    }

    @Override
    public String getEndPointUrl() {
        return "/"+this.getCompanyUUID()+"/vehicles/"+this.getVehicleUUID()+"/locations";
    }

    @Override
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl=endPointUrl;
    }

    @Override
    public String getRestInput() {
        return this.getPayloadBody();
    }

    @Override
    public void setRestInput(String restInput) {
        this.restInput=restInput;
    }
}

