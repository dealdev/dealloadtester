package com.dealservices.rest.client.restattributes;

import org.json.JSONObject;

/**
 * Created by deal-04 on 18/08/15.
 */
public class DeliveryTrajectoryFinisher extends RestAction{
    private JSONObject trajectoryJson;
    private String companyUUID;
    private String trajectoryId;
    private double latitude;
    private double longitude;
    private long timeStamp;
    private double altitude;
    private int speed;

    public JSONObject getTrajectoryJson() {
        return trajectoryJson;
    }

    public void setTrajectoryJson(JSONObject trajectoryJson) {
        this.trajectoryJson = trajectoryJson;
    }

    public String getCompanyUUID() {
        return companyUUID;
    }

    public void setCompanyUUID(String companyUUID) {
        this.companyUUID = companyUUID;
    }

    public String getTrajectoryId() {
        return trajectoryId;
    }

    public void setTrajectoryId(String trajectoryId) {
        this.trajectoryId = trajectoryId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String getEndPointUrl() {
        return "/"+this.getTrajectoryJson().getString("companyUuid")+"/trajectories/"+this.getTrajectoryJson().getString("_id")+"/finish";
    }

    @Override
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl=endPointUrl;
    }

    @Override
    public String getRestInput() {
        return "{\"location\":{\"latitude\":"+this.getLatitude()+",\"longitude\":"+this.getLongitude()+"},\"timestamp\":{\"time\":"+this.getTimeStamp()+"},\"altitude\":"+ this.getAltitude()+",\"speed\":"+this.getSpeed()+",\"provider\":\"gps\"}";

    }

    @Override
    public void setRestInput(String restInput) {

    }
}
