package com.dealservices.rest.client.restattributes;

/**
 * Created by deal-04 on 12/08/15.
 */
public class GetOrderTrajectories extends RestAction{
    private String orderId;
    private String companyUUID;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCompanyUUID() {
        return companyUUID;
    }

    public void setCompanyUUID(String companyUUID) {
        this.companyUUID = companyUUID;
    }

    @Override
    public String getEndPointUrl() {
        return "/"+this.getCompanyUUID()+"/orders/"+this.getOrderId()+"/trajectories";
    }

    @Override
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl=endPointUrl;
    }

    @Override
    public String getRestInput() {
        return this.restInput;
    }

    @Override
    public void setRestInput(String restInput) {
        this.restInput=restInput;
    }
}
