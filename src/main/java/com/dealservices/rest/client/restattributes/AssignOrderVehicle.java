package com.dealservices.rest.client.restattributes;

/**
 * Created by deal-04 on 11/08/15.
 */
public class AssignOrderVehicle extends RestAction{
    private String orderId;
    private String companyUUID;
    private String vehicleUUID;

    public String getVehicleUUID() {
        return vehicleUUID;
    }

    public void setVehicleUUID(String vehicleUUID) {
        this.vehicleUUID = vehicleUUID;
    }

    public String getCompanyUUID() {
        return companyUUID;
    }

    public void setCompanyUUID(String companyUUID) {
        this.companyUUID = companyUUID;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getEndPointUrl() {
        if (this.endPointUrl!=null){
            return this.endPointUrl;
        }else{
            return "/"+this.getCompanyUUID()+"/vehicles/"+this.getVehicleUUID()+"/assignOrderRef";
        }
    }

    @Override
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl=endPointUrl;
    }

    @Override
    public String getRestInput() {
        return "{" +
                "\"companyUuid\" : \""+this.getCompanyUUID()+"\"," +
                "\"entityUuid\" : \""+this.getOrderId()+"\"," +             /*orderid*/
                "\"entityClass\" : \"nl.dealservices.api.Order\"" +
                "}";
    }

    @Override
    public void setRestInput(String restInput) {
        this.restInput=restInput;
    }
}
