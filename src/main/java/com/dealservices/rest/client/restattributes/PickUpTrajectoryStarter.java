package com.dealservices.rest.client.restattributes;

import org.json.JSONObject;

/**
 * Created by deal-04 on 12/08/15.
 */
public class PickUpTrajectoryStarter extends RestAction{
    private JSONObject trajectoryJson;
    private String companyUUID;
    private String trajectoryId;
    private double latitude;
    private double longitude;
    private long timeStamp;
    private double altitude;
    private int speed;

    public String getTrajectoryId() {
        return trajectoryId;
    }

    public void setTrajectoryId(String trajectoryId) {
        this.trajectoryId = trajectoryId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public JSONObject getTrajectoryJson() {
        return trajectoryJson;
    }

    public void setTrajectoryJson(JSONObject trajectoryJson) {
        this.trajectoryJson = trajectoryJson;
//        this.setLatitude(trajectoryJson.getJSONObject("path").getJSONObject("origin").getDouble("latitude"));
//        this.setLongitude(trajectoryJson.getJSONObject("path").getJSONObject("origin").getDouble("latitude"));
        this.setTimeStamp(trajectoryJson.getJSONObject("activity").getJSONObject("range").getLong("start"));
        this.setCompanyUUID(trajectoryJson.getString("companyUuid"));
        this.setTrajectoryId(trajectoryJson.getString("_id"));
    }

    public String getCompanyUUID() {
        return companyUUID;
    }

    public void setCompanyUUID(String companyUUID) {
        this.companyUUID = companyUUID;
    }

    @Override
    public String getEndPointUrl() {
        return "/"+this.getTrajectoryJson().getString("companyUuid")+"/trajectories/"+this.getTrajectoryJson().getString("_id")+"/start";
    }

    @Override
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl=endPointUrl;
    }

    @Override
    public String getRestInput() {
        return "{\"location\":{\"latitude\":"+this.getLatitude()+",\"longitude\":"+this.getLongitude()+"},\"timestamp\":{\"time\":"+this.getTimeStamp()+"},\"altitude\":"+ this.getAltitude()+",\"speed\":"+this.getSpeed()+",\"provider\":\"gps\"}";
//        return "{\"location\":{\"latitude\":"+this.getLatitude()+",\"longitude\":"+this.getLongitude()+"},\"timestamp\":{\"time\":"+this.getTimeStamp()+"},\"altitude\":114.0,\"speed\":17,\"provider\":\"gps\"}";
    }

    @Override
    public void setRestInput(String restInput) {
        this.restInput=restInput;
    }
}
