package com.dealservices.rest.client.restattributes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by deal-04 on 07/08/15.
 */
public abstract class RestAction {
    protected boolean success=false;
    protected String method;
    protected JSONObject responseJson;
    protected String baseUrl;
    protected String endPointUrl;
    protected String httpUrl;
    protected String restInput;
    protected String payloadBody;
    protected String authUUID;
    protected String payloadBodyFromFileUrl;
    protected JSONArray responseJsonArray;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public JSONArray getResponseJsonArray() {
        return responseJsonArray;
    }

    public void setResponseJsonArray(JSONArray responseJsonArray) {
        this.responseJsonArray = responseJsonArray;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPayloadBodyFromFileUrl() {
        return payloadBodyFromFileUrl;
    }

    public void setPayloadBodyFromFileUrl(String payloadBodyFromFileUrl) {
        this.payloadBodyFromFileUrl = payloadBodyFromFileUrl;
        try {
            this.setPayloadBody(readFile(this.payloadBodyFromFileUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getAuthUUID() {
        return authUUID;
    }

    public void setAuthUUID(String authUUID) {
        this.authUUID = authUUID;
    }

    public String getPayloadBody() {
        return this.payloadBody;
    }

    public void setPayloadBody(String payloadBody) {
        this.payloadBody = payloadBody;
    }

    public JSONObject getResponseJson() {
        return responseJson;
    }

    public void setResponseJson(JSONObject responseJson) {
        this.responseJson = responseJson;
    }

    public void setResponseJson(String string) {
        setResponseJson(new JSONObject(string));
    }

    public void setResponseJson(BufferedReader br) {
        setResponseJson(outputReader(br));
    }
    public String getResponseJsonString(){
        return getResponseJson().toString();
    }

    public abstract String getEndPointUrl();

    public abstract void setEndPointUrl(String endPointUrl);

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public abstract String getRestInput();

    public abstract void setRestInput(String restInput);

    public String getHttpUrl() {
        return this.getBaseUrl()+this.getEndPointUrl();
    }

    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

    String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

    public String outputReader(BufferedReader br){

        try {
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                try {
                    line = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
