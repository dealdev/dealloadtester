package com.dealservices.rest.client.restattributes;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by deal-04 on 07/08/15.
 */
//set base and endpoint url first
//set payload body
//
public class Order extends RestAction implements Serializable{
    final static Logger logger = Logger.getLogger(Order.class);
    private String companyUUid;
    private String jobId;
    private int orderJobIdInit;
    private int orderJobIdSequence;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getOrderJobIdInit() {
        return orderJobIdInit;
    }

    public void setOrderJobIdInit(int orderJobIdInit) {
        this.orderJobIdInit = orderJobIdInit;
    }

    public int getOrderJobIdSequence() {
        return orderJobIdSequence;
    }

    public void setOrderJobIdSequence(int orderJobIdSequence) {
        this.orderJobIdSequence = orderJobIdSequence;
    }

    public void incrementOrderJobIdSequence(){
        this.orderJobIdSequence++;
    }

    public String getCompanyUUid() {
        return companyUUid;
    }

    public void setCompanyUUid(String companyUUid) {
        this.companyUUid = companyUUid;
    }


    @Override
    public String getEndPointUrl() {
        if(this.endPointUrl!=null){
            return this.endPointUrl;
        }else{
            return "/" + this.companyUUid + "/orders";
        }
    }

    @Override
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    @Override
    public String getRestInput() {
        if(this.restInput!=null){
            return this.restInput;
        }else{
            if(this.getPayloadBody()!=null) {
                if(!this.jobId.equalsIgnoreCase("null")) {
                    String orderJsonString = this.getPayloadBody();
                    JSONObject jsonObject = new JSONObject(orderJsonString);
                    int jobIdSequence = this.getOrderJobIdInit() + this.getOrderJobIdSequence();
                    jsonObject.getJSONObject("extras").put("jobId", this.jobId + jobIdSequence);
                    jsonObject.put("companyUuid",this.getCompanyUUid());
//                    logger.info("Creating order with job id : "+(this.jobId + jobIdSequence) );

                    return jsonObject.toString();
                }else{
                    return this.getPayloadBody();
                }
            }else{
                return null;
            }

        }
    }

    @Override
    public void setRestInput(String restInput) {
        this.restInput=restInput;
    }

    public void setRestInputFromFile(String filepath){
        try {
            this.setRestInput(this.readFile(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
