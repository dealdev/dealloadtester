package com.dealservices.rest.client;

import com.dealservices.rest.client.restattributes.*;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by deal-04 on 19/08/15.
 */
public class DealLoadTester {
    final static Logger logger = Logger.getLogger(DealLoadTester.class);

    public static void main(String [] args){
        boolean keepGoing=true;

        ApplicationContext context = new ClassPathXmlApplicationContext("DealRestActionBeans.xml");

        Session session=  (Session) context.getBean("session");
        RestClient restClient= (RestClient) context.getBean("restClient");
        Login login = (Login) context.getBean("login");
        Order order = (Order) context.getBean("order");
        AssignOrderVehicle assignOrderVehicle= (AssignOrderVehicle) context.getBean("assignOrderVehicle");
        GetOrderTrajectories getOrderTrajectories= (GetOrderTrajectories) context.getBean("getOrderTrajectories");
        PickUpTrajectoryStarter pickUpTrajectoryStarter= (PickUpTrajectoryStarter) context.getBean("pickUpTrajectoryStarter");
        PickUpTrajectoryFinisher pickUpTrajectoryFinisher= (PickUpTrajectoryFinisher) context.getBean("pickUpTrajectoryFinisher");
        DeliveryTrajectoryStarter deliveryTrajectoryStarter= (DeliveryTrajectoryStarter) context.getBean("deliveryTrajectoryStarter");
        DeliveryTrajectoryFinisher deliveryTrajectoryFinisher= (DeliveryTrajectoryFinisher) context.getBean("deliveryTrajectoryFinisher");

        while(keepGoing){


            restClient.doRest(login);
            logger.info("[LOG] Login Output from Server : ");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");
            logger.info("accessTokenUuids = " + login.getResponseJson().getJSONArray("accessTokenUuids").getString(0));

        /*REST ACTION - CREATE ORDER*/
            order.setCompanyUUid(login.getResponseJson().getString("companyUuid"));
            order.setAuthUUID(restClient.getAuthUUID());
            restClient.doRest(order);
            logger.info("[LOG] Create Order Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - ASSIGN ORDER TO VEHICLE*/
            assignOrderVehicle.setCompanyUUID(login.getResponseJson().getString("companyUuid"));
            assignOrderVehicle.setOrderId(order.getResponseJson().getString("_id"));
            restClient.doRest(assignOrderVehicle);
            logger.info("[LOG] ASSIGN Order to Vehicle Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - Get Order Trajectories*/
            getOrderTrajectories.setCompanyUUID(login.getResponseJson().getString("companyUuid"));
            getOrderTrajectories.setOrderId(order.getResponseJson().getString("_id"));
            restClient.doRest(getOrderTrajectories);
            logger.info("[LOG] getOrderTrajectories Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonArray().getJSONObject(0).toString() + "\n \n" + restClient.getRestAction().getResponseJsonArray().getJSONObject(1).toString());

        /*REST ACTION - Trajectories Starter*/
            pickUpTrajectoryStarter.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(0));
            restClient.doRest(pickUpTrajectoryStarter);
            logger.info("[LOG] Starter Pickup Trajectory Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - Trajectories Finisher*/
            pickUpTrajectoryFinisher.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(0));
            restClient.doRest(pickUpTrajectoryFinisher);
            logger.info("[LOG] Finisher Pickup Trajectory Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");

        /*REST ACTION - deliveryTrajectoryStarter*/

            deliveryTrajectoryStarter.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(1));
            restClient.doRest(deliveryTrajectoryStarter);
            logger.info("[LOG] deliveryTrajectoryStarter Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");


        /*REST ACTION - deliveryTrajectoryFinisher*/
            deliveryTrajectoryFinisher.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(1));
            restClient.doRest(deliveryTrajectoryFinisher);
            logger.info("[LOG] deliveryTrajectoryFinisher Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");

            try {

                //Stopping status -> keepGoing
                if((order.getOrderJobIdSequence()+1) < session.getOrderLimit()){

                    order.incrementOrderJobIdSequence();

                    //Waiting for a while before next load
                    logger.info("[LOG] Waiting for : " + session.getSequenceDelay() + " ms");
                    Thread.sleep(session.getSequenceDelay());

                }else{
                    logger.info("[LOG] Finishing sequential test. Number of batch(s) : " + (order.getOrderJobIdSequence()+1) + " order(s)");
                    keepGoing=false;
                }

            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }

    }
}
