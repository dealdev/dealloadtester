package com.dealservices.rest.client.common;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import nl.dealservices.serialization.Views;
import org.jongo.Jongo;
import org.jongo.marshall.jackson.JacksonMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * Load the properties file called on server startup
 * @author Vishal
 *
 */
public class MgmtProperties {

    private static MgmtProperties mgmtProp = new MgmtProperties() ;
    private static final String MONGO_HOST = "mongo.host";
    private static final String MONGO_PORT = "mongo.port";
    private static final String MONGO_DATABASE_NAME = "mongo.database_name";
    public static final String PROPERTIES_DIR = "src/main/resources/" ;
    /**
     * Connection to mongo db.
     */
    private static Jongo jongo ;
    
    /**
     * Mongo client.
     */
    private static MongoClient mongoClient ;
    /**
     * config file name
     */
  //  private static String FILENAME = "config.properties" ;
    
    /**
     * properties object
     */
    Properties prop = null ;
    private MgmtProperties() {
        init() ;
        initJongo();
    }
    
    
    /**
     * initialize the objects and read the file
     * @throws IOException
     */
    private void init() {
        String filename = "config.properties" ;
        File f2 = new File(PROPERTIES_DIR,filename) ;
        System.out.println("config file path is "+f2.getAbsolutePath());
        try (InputStream in = new FileInputStream(f2)) {
            if(in!=null) {
                try {
                    
                    prop = new Properties() ;
                    prop.load(in);
                    in.close();
                    
                    System.out.println("Property File Loaded "+prop);
                }
                catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }
        catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
       
    }

    /**
     * initialize the mongoclient.
     * @return
     */
    private void initJongo() {

        System.out.printf("initializing Mongo database...\n");

        String host = prop.getProperty(MONGO_HOST);
        Integer port = Integer.parseInt(prop.getProperty(MONGO_PORT));
        String databaseName = prop.getProperty(MONGO_DATABASE_NAME);

        

        try {
            mongoClient = new MongoClient(host, port);

            DB db = mongoClient.getDB(databaseName);

            boolean success = true; // db.authenticate(user, password); // TODO add authentication

            if (success) {
                jongo = new Jongo(db, new JacksonMapper.Builder().withView(Views.DatabaseView.class).build());
            }
            else {
                System.out.printf("could not connect to Mongo database `%s` on `%s:%s` with user `%s`\n",
                                  databaseName,
                                  host,
                                  port);
            }
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        }

        if (jongo == null) {
            System.out.printf("no Mongo DB connection could be established!\n");
            System.exit(1);
        }

        System.out.printf("Mongo database initialized\n");

    }


    /**
     * instance method to return the props object.
     * @return
     */
    public static MgmtProperties getInstance() {
       
        return mgmtProp ;
    }

    /**
     * Returns the properties object.
     * @return
     */
    public Properties getProp() {
        return prop;
    }

    
    
    public Jongo getJongo() {
        return jongo;
    }

    public void shutDownMongo() {
        if(mongoClient!=null) {
            mongoClient.close();
        }
    }

    public static void main(String[] args) {
        getInstance();
        System.out.println("loaded...");
    }
}
