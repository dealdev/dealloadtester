package com.dealservices.rest.client.common;

/**
 * Store the credentials for user
 * @author Vishal
 * @since June 2015
 */
public class UserAuth {


    private String userName ;
    
    private String password ;

    private int status ;
    
    public static int ACTIVE = 1 ;
    
    public static int NOT_ACTIVE = 0 ;
    
    
    public String getUserName() {
        return userName;
    }

    
    public void setUserName(String userName) {
        this.userName = userName;
    }

    
    public String getPassword() {
        return password;
    }

    
    public void setPassword(String password) {
        this.password = password;
    }

    
    public int getStatus() {
        return status;
    }


    
    public void setStatus(int status) {
        this.status = status;
    }



    
    
}
