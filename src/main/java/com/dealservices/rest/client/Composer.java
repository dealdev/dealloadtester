package com.dealservices.rest.client;

/**
 * Created by deal-04 on 12/09/15.
 */
public abstract class Composer {
    private int numberOfOrder;
    private int numberOfOrderPerVehicle;

    public abstract Administrator createAdministrator();

    public int getNumberOfOrder() {
        return numberOfOrder;
    }

    public void setNumberOfOrder(int numberOfOrder) {
        this.numberOfOrder = numberOfOrder;
    }

    public int getNumberOfOrderPerVehicle() {
        return numberOfOrderPerVehicle;
    }

    public void setNumberOfOrderPerVehicle(int numberOfOrderPerVehicle) {
        this.numberOfOrderPerVehicle = numberOfOrderPerVehicle;
    }

    public int getNumberOfDriver(){
        int numberOfDriver=numberOfOrder/numberOfOrderPerVehicle;
        if(numberOfOrder%numberOfOrderPerVehicle>0){
            numberOfDriver++;
        }
        return numberOfDriver;
    }
}
