package com.dealservices.rest.client;

import com.dealservices.rest.client.basic.*;
import com.dealservices.rest.client.common.PasswordHash;
import com.dealservices.rest.client.common.UserAuth;
import com.dealservices.rest.client.db.MongoDb;
import com.dealservices.rest.client.restattributes.AssignOrderVehicle;
import com.dealservices.rest.client.restattributes.Login;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import nl.dealservices.api.Order;
import nl.dealservices.api.Vehicle;
import nl.dealservices.rest.client.OrderService;
import nl.dealservices.rest.client.ServiceSession;
import nl.dealservices.rest.client.VehicleService;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import retrofit.RetrofitError;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by deal-04 on 02/09/15.
 */
public class Administrator extends Login implements Runnable{
    final static Logger logger = Logger.getLogger(Administrator.class);
    private AdministratorSession administratorSession;
    private com.dealservices.rest.client.restattributes.Order order;
    private AssignOrderVehicle assignOrderVehicle;
    private VehicleModel vehicleModel;
    private UserModel userModel;
    private Driver driverModel;
    private OrderModel orderModel;
    private RestClient restClient= new RestClient();
    private List <UserModel> userModelList;
    private List <Driver> driverList= new ArrayList<>();
    private  List <Order> orderList= new ArrayList<>();
    private int numberOfOrders;
    private int numberOfOrdersPerVehicle;
    private int maxTry=3;

    public void setsUp(){
        //Creates User and Vehicle, also assign the vehicle
        this.createUserModelList();
        //creates Orders
        this.createOrderList();
        //Assign orders to vehicle
        this.assignOrdersToVehicles();

        setsUpDriver();

        driverModel.login();

        driverModel.doAllOrder();
    }

    public void setsUpDriver(){
        for(UserModel userModel:userModelList){
            driverModel.setUserModel(userModel);
        }
    }

    @Override
    public void run() {
        setsUp();
    }
    public void login(){
        if(this.getUserName()==null||this.getPassWord()==null||this.getHttpUrl()==null){
            throw new NullPointerException();
        }else{
            restClient.doRest(this);
        }
    }

    public UserModel createDriver(){
        UserModel cloned=null;
        try {
            UUID userRandomId = UUID.randomUUID();
            cloned=org.apache.commons.lang3.SerializationUtils.clone(this.userModel);
            cloned.setUserName(this.userModel.getUserName() + userRandomId.toString());
            cloned.setEmailAddress(cloned.getUserName()+cloned.getEmailAddress());
        }catch (RuntimeException  re){
            logger.error("Runtime Error creating Driver,"+re.getMessage());
            System.exit(-1);
        }catch (Exception e){
            logger.error("Error creating Driver" + e.getMessage());
            return null;
        }
        return this.createUser(cloned);
    }

    public void createUserModelList(){
        //creates driver and vehicle and assign driver to vehicle
        logger.info("Start creating Drivers and Vehicles...");
        this.userModelList=new ArrayList<>();
        try {
            for (int i = 0; i < this.getNumberOfVehicle(); i++) {
                int driverTrial=0;
                int vehicleTrial=0;
                int assignTrial=0;
                UserModel userModel=null;
                VehicleModel vehicleModel=null;
                //create Driver
                while (true) {
                    if(driverTrial >=maxTry){
                        logger.error("Failed creates driver more than 3 times");
                        throw new RuntimeException("Failed creates vehicle or driver more than 3 times");
                    }
                    //creating driver
                    userModel= this.createDriver();
                    if(userModel!=null){
                        if(userModel.isCreated()){
                            break;
                        }else{
                            driverTrial++;
                        }
                    }else{
                        driverTrial++;
                    }
                }
                //create Vehicle
                while(true){
                    if(vehicleTrial >=maxTry){
                        logger.error("Failed creates vehicle more than 3 times");
                        throw new RuntimeException("Failed creates vehicle or driver more than 3 times");
                    }
                    vehicleModel=this.createVehicle();
                    if(vehicleModel!=null){
                        if(vehicleModel.getVehicle()!=null){
                            break;
                        }else{
                            vehicleTrial++;
                        }
                    }else{
                        vehicleTrial++;
                    }
                }
                //and assign it
                while(true){
                    if(assignTrial >=maxTry){
                        logger.error("Failed creates vehicle more than 3 times");
                        throw new RuntimeException("Failed creates vehicle or driver more than 3 times");
                    }
                    userModel=this.assignDrivertoVehicle(userModel,vehicleModel.getVehicle());

                    if(userModel.getAssignedVehicle()!=null){
                        break;
                    }else {
                        assignTrial++;
                    }
                }
                //and add it to the array list of user that have been assigned with the vehicle successfully
                this.userModelList.add(userModel);
            }

        }catch (RuntimeException re){
            logger.error("Runtime Error creating Driver or vehicle,"+re.getMessage());
            System.exit(-1);
        }
    }

    public UserModel assignDrivertoVehicle(UserModel driver, Vehicle vehicle){
        //assign driver to vehicle
        Client client = Client.create() ;
        WebResource webResource = client.resource(this.getBaseUrl() + "/" + this.getCompanyUUID() + "/vehicles/"+vehicle.getUuid()+"/currentDriverRef");
        webResource.header("AuthTokens", this.getAuthUUID());
        String jsonString="{\"companyUuid\":\""+this.getCompanyUUID()+"\",\"entityUuid\":\""+driver.getUserUUID()+"\",\"entityClass\":\"nl.dealservices.api.User\"}\n";
        //assign vehicle to driver
        Client client2 = Client.create() ;
        WebResource webResource2 = client2.resource(this.getBaseUrl() + "/" + this.getCompanyUUID() + "/users/"+driver.getUserUUID()+"/extras/add");
        webResource2.header("AuthTokens", this.getAuthUUID());
        String jsonString2="{\"vehicleUuid\":\""+vehicle.getUuid()+"\"}";
        try{
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("AuthTokens", this.getAuthUUID())
                    .header("JSESSIONID", this.getJSESSIONID()).put(ClientResponse.class, jsonString);
            ClientResponse response2 = webResource2.type(MediaType.APPLICATION_JSON).header("AuthTokens", this.getAuthUUID())
                    .header("JSESSIONID", this.getJSESSIONID()).put(ClientResponse.class, jsonString2);

            if (response.getStatus() != 200||response2.getStatus() != 200) {
                if(response.getStatus()!=200) {
                    String errorMsg = "assign driver to vehicle is failed, got Error Response from Server.... \n  response : "+ response.toString();
                    logger.error(errorMsg);
                }
                if (response2.getStatus()!=200){
                    String errorMsg = "assign vehicle to driver is failed, got Error Response from Server.... \n  response : "+ response2.toString();
                    logger.error(errorMsg);
                }
            }else{
                driver.setAssignedVehicle(vehicle);
                logger.info("driver ["+driver.getUserName()+"] is assigned to vehicle "+vehicle.getUuid());
            }
        }catch(Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return driver;
    }

    public UserModel createUser(UserModel userModel){
        logger.info("Creating User");
        Client client = Client.create() ;
        WebResource webResource = client.resource(this.getBaseUrl() + "/" + this.getCompanyUUID() + "/" + "users");
        webResource.header("AuthTokens", this.getAuthUUID());
        try{
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("AuthTokens", this.getAuthUUID())
                    .header("JSESSIONID", this.getJSESSIONID()).post(ClientResponse.class, userModel.getJson()  );
            if (response.getStatus() != 200) {
                String errorMsg = String.format("Error Response from Server... %s. \n  user json is %s ",
                        response.toString(),
                        userModel.getJson());
                logger.error("Create User is failed, "+errorMsg);
            }else{
                //Set created status
                userModel.setCreated(true);
                final InputStream entityInputStream = response.getEntityInputStream();

                //Set user uuid & set json response from server
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(entityInputStream, "UTF-8"));
                StringBuilder responseStrBuilder = new StringBuilder();

                String inputStr;
                while ((inputStr = streamReader.readLine()) != null)
                    responseStrBuilder.append(inputStr);
                String jsonString=responseStrBuilder.toString();
                JSONObject jsonObject=new JSONObject(jsonString);
                String userUUID=jsonObject.getString("_id");
                userModel.setUserUUID(userUUID);
                userModel.setJsonCreateUserResponse(jsonString);

                //
                logger.info("user created successfully {} "+ response);
            }

        }catch(Exception e) {
            e.printStackTrace();
        }
        // if user creation is successful then store the auth info to mongo db.
        if (userModel.isCreated()){
            MongoDb mongo = new MongoDb() ;
            UserAuth uAuth = new UserAuth();
            uAuth.setPassword(PasswordHash.hash(userModel.getPassword()));
            uAuth.setUserName(userModel.getUserName());
            uAuth.setStatus(UserAuth.ACTIVE);
            mongo.insertUserAuth(uAuth);
        }
        return userModel;
    }

//    public Order createOrder(Order order){
//        Order clonedOrder=null;
//        this.restClient.doRest(order);
//        if(order.isSuccess()){
//            logger.info("created order with id : " + order.getJobId());
//            clonedOrder=org.apache.commons.lang3.SerializationUtils.clone(order);
//        }else{
//            logger.info("Failed create order with id : " + order.getJobId());
//        }
//        return clonedOrder;
//    }

//    public nl.dealservices.api.Order createOrder

//    public Order createOrder(JobModel jobModel) {
//        jobModel.getExtras().put("jobId", jobModel.getJobId());
//        String orderJson = jobModel.getJsonString();
//
//        Client client = Client.create() ;
//        WebResource webResource = client.resource(this.getBaseUrl() + "/" + this.getCompanyUUID() + "/" + "orders"); //+"/"+companyUuid+"/"+"orders"
//
//        logger.debug("going to create job.  URI is " + webResource.getURI());
//        ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("AuthTokens", this.getAuthUUID())
//                .header("JSESSIONID", this.getJSESSIONID()).post(ClientResponse.class, orderJson);
//        if (response.getStatus() != 200) {
//            String errorMsg = String
//                    .format("Error Response from Server... %s. \n  authtoken is %s \n order json is %s ",
//                            response.toString(),
//                            this.getAuthUUID(),
//                            orderJson);
//            logger.error(errorMsg);
//            return null;
//        }else {
//
//            jobModel.setCreated(true);
//        }
//        Order order=response.getEntity(Order.class);
//        return order;
//    }

    public Order createOrder(Order order){
        logger.info("Creating Order");
        ServiceSession serviceSession = new ServiceSession(this.baseUrl);
        serviceSession.withTokenAndSessionId(this.getAuthUUID(), this.getJSESSIONID());
        OrderService orderService = serviceSession.createService(OrderService.class);
        Order orderResult=null;
        try {
            orderResult= orderService.createOrder(this.getCompanyUUID(), order);
        }catch (RetrofitError re){
            return null;
        }
        return orderResult;
    }

    public Order createOrder(OrderModel orderModel){
        logger.info("Creating Order");
        restClient.setJSESSIONID(this.JSESSIONID);
        restClient.setAuthUUID(this.authUUID);
        order.setCompanyUUid(this.getCompanyUUID());
        order.setAuthUUID(this.getAuthUUID());
        restClient.doRest(order);

        if (restClient.getHttpResponse()==200){
            Order order = new Gson().fromJson(restClient.getRestAction().getResponseJson().toString(), Order.class);

            return order;
        }else{
            return null;
        }
    }

    public void createOrderList(){
        Order orderResult;

        int orderIdx =0;
//        for(int i =0; i<this.getNumberOfOrders();i++){
        while(true){
            int ordertrial=0;
            if(orderIdx>=numberOfOrders){
                break;
            }
            while(true) {
                if(ordertrial >= maxTry){
                    logger.error("Failed to create order more than "+maxTry+" time(s)");
                    System.exit(-1);
                }
                orderResult=createOrder(this.getOrderModel());
                if (orderResult != null) {
                    //Adding to List Order
                    this.orderList.add(orderResult);
                    orderIdx++;
                    break;
                } else {
                    ordertrial++;
                }
            }
        }
    }

    public Order assignOrderToVehicle(Order order,UserModel userModel){
        logger.info("Assigning order : " + order.getUuid() + ", with vehicle : " + userModel.getAssignedVehicle().getUuid());
        ServiceSession serviceSession = new ServiceSession(this.baseUrl);
        serviceSession.withTokenAndSessionId(this.getAuthUUID(), this.getJSESSIONID());
        VehicleService vehicleService=serviceSession.createService(VehicleService.class);

        try {
            Order orderResult=vehicleService.assignOrder(this.getCompanyUUID(), userModel.getAssignedVehicle().getUuid(), order);
        }catch (RetrofitError retrofitError){
            logger.error("failed assign order to vehicle, response : "+ retrofitError.getMessage());
            return null;
        }
        logger.info("Successfully assigned order : "+order.getUuid()+", with vehicle : "+ userModel.getAssignedVehicle().getUuid());
        return order;
    }

    public Order assignOrderTovehicle(Order order,UserModel userModel){
        restClient.setJSESSIONID(this.JSESSIONID);
        restClient.setAuthUUID(this.authUUID);
//        AssignOrderVehicle assignOrderVehicle= new AssignOrderVehicle();
//        assignOrderVehicle.setMethod("post");
//        assignOrderVehicle.setBaseUrl(this.getBaseUrl());
        assignOrderVehicle.setVehicleUUID(userModel.getAssignedVehicle().getUuid());
        assignOrderVehicle.setCompanyUUID(this.getCompanyUUID());
        assignOrderVehicle.setOrderId(order.getUuid());
        restClient.doRest(assignOrderVehicle);
        if(restClient.getHttpResponse()==200){
            logger.info("[LOG] ASSIGN Order to Vehicle Output from Server .... \n");
            logger.info(restClient.getRestAction().getResponseJsonString() + "\n");
        }else{
            return null;
        }
        return order;
    }

    public void assignOrdersToVehicles(){
        int userModelidx=0;
        int numberOfVehicle=this.getNumberOfVehicle();
        for(nl.dealservices.api.Order order:this.getOrderList()){
            userModelidx++;
            UserModel userModel =this.getUserModelList().get(userModelidx % numberOfVehicle);
            Order orderResult=this.assignOrderTovehicle(order, userModel);
            if(orderResult!=null){
                userModel.getOrderTodo().add(orderResult);
            }else{
                logger.error("order "+order.getUuid()+" is failed to be assigned to vehicle"+userModel.getAssignedVehicle().getUuid());
                throw new RuntimeException("order "+order.getUuid()+" is failed to be assigned to vehicle"+userModel.getAssignedVehicle().getUuid());
            }
        }
    }

    public VehicleModel createVehicle(){
        logger.info("creating vehicle..");
        Vehicle vehicleResult=null;
        VehicleModel vehicleModelCloned =org.apache.commons.lang3.SerializationUtils.clone(vehicleModel);

        Vehicle vehicle = null;
        try {
            vehicle = new ObjectMapper().readValue(vehicleModelCloned.getJson(), nl.dealservices.api.Vehicle.class);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Failure when creating vehicle, error : " + e.getMessage());
            return null;
        }
        vehicleModelCloned.setVehicle(vehicle);

        ServiceSession serviceSession = new ServiceSession(this.baseUrl);
        serviceSession.withTokenAndSessionId(this.getAuthUUID(), this.getJSESSIONID());
        VehicleService service = serviceSession.createService(VehicleService.class);

        try {
            vehicleResult= service.createVehicle(this.getCompanyUUID(), vehicle);
        }catch (RetrofitError re){
            re.printStackTrace();
            logger.error("Failure when creating vehicle, error : "+re.getMessage());
            vehicleResult=null;
        }

        vehicleModelCloned.setVehicle(vehicleResult);
        return vehicleModelCloned;
    }

    public List<nl.dealservices.api.Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<nl.dealservices.api.Order> orderList) {
        this.orderList = orderList;
    }

    public List<UserModel> getUserModelList() {
        return userModelList;
    }

    public void setUserModelList(List<UserModel> userModelList) {
        this.userModelList = userModelList;
    }

    public RestClient getRestClient() {
        return restClient;
    }

    public void setRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    public VehicleModel getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(VehicleModel vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }


    public int getNumberOfOrders() {
        return numberOfOrders;
    }

    public void setNumberOfOrders(int numberOfOrders) {
        this.numberOfOrders = numberOfOrders;
    }

    public void setOrderperDriver(int n){
        this.numberOfOrders=n;
        this.numberOfOrdersPerVehicle=n;
    }

    public int getNumberOfOrdersPerVehicle() {
        return numberOfOrdersPerVehicle;
    }

    public void setNumberOfOrdersPerVehicle(int numberOfOrdersPerVehicle) {
        this.numberOfOrdersPerVehicle = numberOfOrdersPerVehicle;
    }

    public int getNumberOfVehicle(){
        int retVal;
        retVal=this.getNumberOfOrders()/this.getNumberOfOrdersPerVehicle();
        if(this.getNumberOfOrders() % this.getNumberOfOrdersPerVehicle()>0){
            retVal++;
        }
        return retVal;
    }

    public int getMaxTry() {
        return maxTry;
    }

    public void setMaxTry(int maxTry) {
        this.maxTry = maxTry;
    }

    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }

    public Driver getDriverModel() {
        return driverModel;
    }

    public void setDriverModel(Driver driverModel) {
        this.driverModel = driverModel;
    }

    public OrderModel getOrderModel() {
        return orderModel;
    }

    public void setOrderModel(OrderModel orderModel) {
        this.orderModel = orderModel;
    }

    public AdministratorSession getAdministratorSession() {
        return administratorSession;
    }

    public void setAdministratorSession(AdministratorSession administratorSession) {
        this.administratorSession = administratorSession;
        super.JSESSIONID=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getJSESSIONID());
        super.authUUID=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getAuthUUID());
        super.userName=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getUserName());
        super.passWord=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getPassWord());
        super.companyUUID=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getCompanyUUID());
        super.endPointUrl=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getEndPointUrl());
        super.baseUrl=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getBaseUrl());
        super.method=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getMethod());
        super.payloadBody=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.getPayloadBody());
        super.success=org.apache.commons.lang3.SerializationUtils.clone(administratorSession.isSuccess());
    }

    public com.dealservices.rest.client.restattributes.Order getOrder() {
        return order;
    }

    public void setOrder(com.dealservices.rest.client.restattributes.Order order) {
        this.order = order;
    }

    public AssignOrderVehicle getAssignOrderVehicle() {
        return assignOrderVehicle;
    }

    public void setAssignOrderVehicle(AssignOrderVehicle assignOrderVehicle) {
        this.assignOrderVehicle = assignOrderVehicle;
    }
}


