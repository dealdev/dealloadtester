package com.dealservices.rest.client.basic;

import com.fasterxml.jackson.databind.JsonMappingException;
import nl.dealservices.api.Order;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by deal-04 on 11/09/15.
 */
public class OrderModel implements Serializable{
    private Order order;
    private String jsonString;
    private String payloadBodyFromFileUrl;

    public void setPayloadBodyFromFileUrl(String payloadBodyFromFileUrl) {
        this.payloadBodyFromFileUrl = payloadBodyFromFileUrl;
        try {
            this.setJsonString(readFile(this.payloadBodyFromFileUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Set StringJSON to object
        try {
            String jsonStr=this.getJsonString();
            order=new ObjectMapper().readValue(jsonStr, Order.class);
        } catch (JsonMappingException e){
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;

        try {
            String jsonStr=jsonString;
            order=new ObjectMapper().readValue(jsonStr, Order.class);

        } catch (JsonMappingException e){
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getPayloadBodyFromFileUrl() {
        return payloadBodyFromFileUrl;
    }

}


