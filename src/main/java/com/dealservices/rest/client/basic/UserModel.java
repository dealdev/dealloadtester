package com.dealservices.rest.client.basic;

import nl.dealservices.api.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deal-04 on 04/09/15.
 */
public class UserModel implements Serializable{

    public final static int versionCode=1;
    public final static String className ="nl.dealservices.api.User";
    private List<Order> orderTodo = new ArrayList<>();
    private Vehicle assignedVehicle;
    private boolean created=false;
    private String userUUID;
    private String companyUuid;
    private String userName;
    private String name;
    private String password;
    private String emailAddress;
    private String phoneNumber;
    private String driverLicenseNumber;
    private Role role;
    private String jsonCreateUserResponse;

    public UserModel(){
        super();
    }

    public String getJsonCreateUserResponse() {
        return jsonCreateUserResponse;
    }

    public void setJsonCreateUserResponse(String jsonCreateUserResponse) {
        this.jsonCreateUserResponse = jsonCreateUserResponse;
    }

    public Vehicle getAssignedVehicle() {
        return assignedVehicle;
    }

    public void setAssignedVehicle(Vehicle assignedVehicle) {
        this.assignedVehicle = assignedVehicle;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role roles) {
        this.role = roles;
    }

    public enum Role {
        COORDINATOR, DRIVER, SENDER, RECIPIENT, ADMINISTRATOR, CUSTOMER
    }
    public String getJson (){
//        return "{\"versionCode\":1"+versionCode+",\n" +
//                "  \"className\":\""+className+"\",\n" +
//                "  \"extras\":{\n" +
//                "    \"driverLicense\":\""+driverLicenseNumber+"\",\n" +
//                "    \"createDate\":\""+new Date().toString()+"\"\n" +
//                "  },\n" +
//                "  \"companyUuid\":null,\n" +
//                "  \"username\":\""+userName+"\",\n" +
//                "  \"name\":\""+name+"\",\n" +
//                "  \"additionalCompanyRefs\":[],\n" +
//                "  \"phoneNumber\":\""+phoneNumber+"\",\n" +
//                "  \"emailAddress\":\""+emailAddress+"\",\n" +
//                "  \"accessTokenUuids\":null,\n" +
//                "  \"role\":\""+role+"\",\n" +
//                "  \"_id\":null\n" +
//                "}";

        return "{\"username\":\""+userName+"\",\"role\":\""+role+"\",\"name\":\""+name+"\",\"emailAddress\":\""+emailAddress+"\",\"phoneNumber\":\""+phoneNumber+"\",\"extras\":{\"driverLicense\":\""+driverLicenseNumber+"\",\"password\":\""+password+"\"}}";
    }

    @Override
    public String toString(){
        return this.getJson();
    }


    public UserModel(String userName){
        this.userName=userName;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getCompanyUuid() {
        return companyUuid;
    }

    public void setCompanyUuid(String companyUuid) {
        this.companyUuid = companyUuid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public List<Order> getOrderTodo() {
        return orderTodo;
    }

    public void setOrderTodo(List<Order> orderTodo) {
        this.orderTodo = orderTodo;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public final static class Users {
        public static final String Username = "userName" ;
        public static final String Role = "role" ;

        public static final String EmailAddress = "emailAddress" ;
        public static final String Name = "name" ;

        public static final String DriverLicenseNumber = "driverLicenseNumber" ;
        public static final String PhoneNumber = "phoneNumber" ;
        public static final String Password = "password" ;
    }
}