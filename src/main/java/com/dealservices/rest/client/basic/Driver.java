package com.dealservices.rest.client.basic;

import com.dealservices.rest.client.RestClient;
import com.dealservices.rest.client.restattributes.*;
import nl.dealservices.api.Order;
import org.apache.log4j.Logger;

/**
 * Created by deal-04 on 02/09/15.
 */
public class Driver extends Login implements Runnable{
    Logger logger=Logger.getLogger(Driver.class);
    private RestClient restClient= new RestClient();
    private UserModel userModel;
    private Login login;
    private PickUpTrajectoryStarter pickUpTrajectoryStarter;
    PickUpTrajectoryFinisher pickUpTrajectoryFinisher;
    DeliveryTrajectoryStarter deliveryTrajectoryStarter;
    DeliveryTrajectoryFinisher deliveryTrajectoryFinisher;

    public void login(){

        if(this.getUserName()==null||this.getPassWord()==null||this.getHttpUrl()==null){
            throw new NullPointerException();
        }else{
            restClient.doRest(this);
        }
    }

    @Override
    public void run() {
        login();
    }

    public Driver(){
        super();
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
        this.setUserName(userModel.getUserName());
        this.setPassWord(userModel.getPassword());
    }

    public RestClient getRestClient() {
        return restClient;
    }

    public void setRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
        this.setMethod(org.apache.commons.lang3.SerializationUtils.clone(login.getMethod()));
        this.setBaseUrl(org.apache.commons.lang3.SerializationUtils.clone(login.getBaseUrl()));
        this.setEndPointUrl(org.apache.commons.lang3.SerializationUtils.clone(login.getEndPointUrl()));
    }

//    public void getOrderTrajectories(Order order){
//        ServiceSession serviceSession = new ServiceSession(this.baseUrl);
//        serviceSession.withTokenAndSessionId(this.getAuthUUID(), this.getJSESSIONID());
//        TrajectoryService service = serviceSession.createService(TrajectoryService.class);
//        List<Trajectory>trajectories=null;
//        try {
//            trajectories=service.generateTrajectory(this.getCompanyUUID(), order.getUuid());
//        }catch (RetrofitError re){
//            logger.error(re.getMessage());
//        }
//
//        if(trajectories!=null){
//            for(Trajectory trajectory:trajectories){
//                logger.debug("got order trajectories successfully, trajectoriy id : "+trajectory.getUuid());
//            }
//        }
//
//    }

    public GetOrderTrajectories getOrderTrajectory(Order order){
        logger.info("getting order trajectories, from order : " + order.getUuid());
        GetOrderTrajectories getOrderTrajectories=new GetOrderTrajectories();
        getOrderTrajectories.setMethod("get");
        getOrderTrajectories.setBaseUrl(this.getBaseUrl());
        getOrderTrajectories.setCompanyUUID(this.getCompanyUUID());
        getOrderTrajectories.setOrderId(order.getUuid());

        restClient.doRest(getOrderTrajectories);
        logger.info("[LOG] getOrderTrajectories Output from Server .... \n");
        getOrderTrajectories.setResponseJsonArray(restClient.getRestAction().getResponseJsonArray());
        logger.info(getOrderTrajectories.getResponseJsonArray().getJSONObject(0).toString() + "\n \n" + restClient.getRestAction().getResponseJsonArray().getJSONObject(1).toString());

        if(getOrderTrajectories.getResponseJsonArray().length()>0){
            return getOrderTrajectories;
        }else{
            throw new RuntimeException("trajectories are null, from order id : "+order.getUuid());
        }
    }

    public void startPickup(GetOrderTrajectories getOrderTrajectories){
        logger.info("[LOG] startPickup  : ");
        pickUpTrajectoryStarter.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(0));
        restClient.doRest(pickUpTrajectoryStarter);
        logger.info("[LOG] Starter Pickup Trajectory Output from Server : ");
        logger.info(restClient.getRestAction().getResponseJsonString() + "");
    }

    public void finishPickup(GetOrderTrajectories getOrderTrajectories){
        logger.info("[LOG] finishPickup : ");
        pickUpTrajectoryFinisher.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(0));
        restClient.doRest(pickUpTrajectoryFinisher);
        logger.info("[LOG] Finisher Pickup Trajectory Output from Server : ");
        logger.info(restClient.getRestAction().getResponseJsonString() + "");
    }

    public void startDelivery(GetOrderTrajectories getOrderTrajectories){
        logger.info("[LOG] startDelivery  : ");
        deliveryTrajectoryStarter.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(1));
        restClient.doRest(deliveryTrajectoryStarter);
        logger.info("[LOG] deliveryTrajectoryStarter Output from Server : ");
        logger.info(restClient.getRestAction().getResponseJsonString() + "");
    }

    public void finishDelivery(GetOrderTrajectories getOrderTrajectories){
        logger.info("[LOG] finishDelivery : ");
        deliveryTrajectoryFinisher.setTrajectoryJson(getOrderTrajectories.getResponseJsonArray().getJSONObject(1));
        restClient.doRest(deliveryTrajectoryFinisher);
        logger.info("[LOG] deliveryTrajectoryFinisher Output from Server :");
        logger.info(restClient.getRestAction().getResponseJsonString() + "");
    }

    public void doAnOrder(Order order){
        GetOrderTrajectories getOrderTrajectories=getOrderTrajectory(order);
        startPickup(getOrderTrajectories);
        finishPickup(getOrderTrajectories);
        startDelivery(getOrderTrajectories);
        finishDelivery(getOrderTrajectories);
    }

    public void doAllOrder(){
        for(Order order:this.getUserModel().getOrderTodo()){
            doAnOrder(order);
        }
    }

    public PickUpTrajectoryStarter getPickUpTrajectoryStarter() {
        return pickUpTrajectoryStarter;
    }

    public void setPickUpTrajectoryStarter(PickUpTrajectoryStarter pickUpTrajectoryStarter) {
        this.pickUpTrajectoryStarter = pickUpTrajectoryStarter;
    }

    public PickUpTrajectoryFinisher getPickUpTrajectoryFinisher() {
        return pickUpTrajectoryFinisher;
    }

    public void setPickUpTrajectoryFinisher(PickUpTrajectoryFinisher pickUpTrajectoryFinisher) {
        this.pickUpTrajectoryFinisher = pickUpTrajectoryFinisher;
    }

    public DeliveryTrajectoryStarter getDeliveryTrajectoryStarter() {
        return deliveryTrajectoryStarter;
    }

    public void setDeliveryTrajectoryStarter(DeliveryTrajectoryStarter deliveryTrajectoryStarter) {
        this.deliveryTrajectoryStarter = deliveryTrajectoryStarter;
    }

    public DeliveryTrajectoryFinisher getDeliveryTrajectoryFinisher() {
        return deliveryTrajectoryFinisher;
    }

    public void setDeliveryTrajectoryFinisher(DeliveryTrajectoryFinisher deliveryTrajectoryFinisher) {
        this.deliveryTrajectoryFinisher = deliveryTrajectoryFinisher;
    }
}
