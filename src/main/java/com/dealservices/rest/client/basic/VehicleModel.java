package com.dealservices.rest.client.basic;

import nl.dealservices.api.Vehicle;

import java.io.Serializable;

/**
 * Created by deal-04 on 02/09/15.
 */
public class VehicleModel implements Serializable{
    private String vehicleName;
    private String Owner;
    private String type;
    private String Width;
    private String Height;
    private String Depth;
    private String baseAddressnumber;
    private String baseAddresspostcode;
    private String baseAddresscountry;
    private String baseAddresscity;
    private String baseAddressstreet;
    private String coordinates;
    private String plateNumber;
    private String licenseNumber;
    private Vehicle vehicle;

    public  String getJson(){
//        return "{"+"\"lastKnownLocation\": {\"location\" : {\"latitude\": -6.8876498,\"longitude\": 107.6005169}},"+"\"baseAddress\":{\"country\":\""+baseAddresscountry+"\",\"city\":\""+baseAddresscity+"\",\"street\":\""+baseAddressstreet+"\",\"number\":\""+baseAddressnumber+"\",\"postcode\":\""+baseAddresspostcode+"\",\"location\":{\"type\":\"Point\",\"coordinates\": "+coordinates+" }},\"extras\":{\"type\":\""+type+"\",\"vehicleName\":\""+vehicleName+"\",\"vehicleOwner\":\""+Owner+"\",\"plateNumber\":\""+plateNumber+"\",\"licenseNumber\":\""+licenseNumber+"\"},\"capacity\":{\"width\":"+Width+",\"height\":"+Height+",\"depth\":"+Depth+"}}";
        return "{"+"\"lastKnownLocation\": {\n" +
                "      \"location\": {\n" +
                "        \"latitude\": 51.9087952,\n" +
                "        \"longitude\": 4.4478653\n" +
                "      },\n" +
                "      \"timestamp\": {\n" +
                "        \"time\": 1440521013628,\n" +
                "        \"string\": \"25-08-2015 16:43:33\"\n" +
                "      },\n" +
                "      \"bearing\": null,\n" +
                "      \"altitude\": 0,\n" +
                "      \"speed\": 0,\n" +
                "      \"provider\": \"fused\"\n" +
                "    },"+"\"baseAddress\":{\"country\":\""+baseAddresscountry+"\",\"city\":\""+baseAddresscity+"\",\"street\":\""+baseAddressstreet+"\",\"number\":\""+baseAddressnumber+"\",\"postcode\":\""+baseAddresspostcode+"\",\"location\":{\"type\":\"Point\",\"coordinates\": "+coordinates+" }},\"extras\":{\"type\":\""+type+"\",\"vehicleName\":\""+vehicleName+"\",\"vehicleOwner\":\""+Owner+"\",\"plateNumber\":\""+plateNumber+"\",\"licenseNumber\":\""+licenseNumber+"\"},\"capacity\":{\"width\":"+Width+",\"height\":"+Height+",\"depth\":"+Depth+"}}";
    }

//    public static void main(String []args){
//        nl.dealservices.api.Vehicle v  = null;
//        try {
//            v = new ObjectMapper().readValue(getJson(), nl.dealservices.api.Vehicle.class);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println(v);
//    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        Owner = owner;
    }

//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getDepth() {
        return Depth;
    }

    public void setDepth(String depth) {
        Depth = depth;
    }

    public String getBaseAddressnumber() {
        return baseAddressnumber;
    }

    public void setBaseAddressnumber(String baseAddressnumber) {
        this.baseAddressnumber = baseAddressnumber;
    }

    public String getBaseAddresspostcode() {
        return baseAddresspostcode;
    }

    public void setBaseAddresspostcode(String baseAddresspostcode) {
        this.baseAddresspostcode = baseAddresspostcode;
    }

    public String getBaseAddresscountry() {
        return baseAddresscountry;
    }

    public void setBaseAddresscountry(String baseAddresscountry) {
        this.baseAddresscountry = baseAddresscountry;
    }

    public String getBaseAddresscity() {
        return baseAddresscity;
    }

    public void setBaseAddresscity(String baseAddresscity) {
        this.baseAddresscity = baseAddresscity;
    }

    public String getBaseAddressstreet() {
        return baseAddressstreet;
    }

    public void setBaseAddressstreet(String baseAddressstreet) {
        this.baseAddressstreet = baseAddressstreet;
    }
}
