package com.dealservices.rest.client.basic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import nl.dealservices.api.Address;
import nl.dealservices.api.Interval;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by deal-04 on 07/09/15.
 */
public class JobModel {
    private boolean created=false;
    private String companyUuid;
    private String description ;
    protected String payloadBodyFromFileUrl;
    private Address pickUpAddress ;
    private Address deliveryAddress  ;
    private Interval pickUpTimeInterval ;
    private Interval deliveryTimeInterval ;
    private Long loadingDuration ;
    private Long unloadingDuration ;
    private String iconUrl ;
    private String jsonString;
    @JsonIgnore
    private String vehicleName ;

    @JsonIgnore
    private String jobId ;

    private HashMap<String, String> extras = new HashMap<String, String>() ;

    public JobModel() {
        pickUpAddress = new Address() ;
        deliveryAddress = new Address() ;
        extras = new HashMap<String, String>() ;
        pickUpTimeInterval = new Interval() ;
        deliveryTimeInterval = new Interval() ;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public String getCompanyUuid() {
        return companyUuid;
    }

    public void setCompanyUuid(String companyUuid) {
        this.companyUuid = companyUuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Address getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(Address pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }
    public Interval getPickUpTimeInterval() {
        return pickUpTimeInterval;
    }

    public void setPickUpTimeInterval(Interval pickUpTimeInterval) {
        this.pickUpTimeInterval = pickUpTimeInterval;
    }

    public Interval getDeliveryTimeInterval() {
        return deliveryTimeInterval;
    }

    public void setDeliveryTimeInterval(Interval deliveryTimeInterval) {
        this.deliveryTimeInterval = deliveryTimeInterval;
    }
    public Long getLoadingDuration() {
        return loadingDuration;
    }

    public void setLoadingDuration(Long loadingDuration) {
        // convert to millisecond
        this.loadingDuration = loadingDuration * 60 * 1000;
    }

    public Long getUnloadingDuration() {
        return unloadingDuration;
    }

    public void setUnloadingDuration(Long unloadingDuration) {
        this.unloadingDuration = unloadingDuration * 60 * 1000;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public HashMap<String, String> getExtras() {
        return extras;
    }

    public void setExtras(HashMap<String, String> extras) {
        this.extras = extras;
    }

    public void addToExtras(String name, String value) {
        extras.put(name, value) ;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getPayloadBodyFromFileUrl() {
        return payloadBodyFromFileUrl;
    }

    public void setPayloadBodyFromFileUrl(String payloadBodyFromFileUrl) {
        this.payloadBodyFromFileUrl = payloadBodyFromFileUrl;
        try {
            this.setJsonString(readFile(this.payloadBodyFromFileUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String toString() {
        return "JobModel [companyUuid=" + companyUuid + ", description=" + description + ", pickupAddress=" +
                pickUpAddress + ", deliveryAddress=" + deliveryAddress + ", pickUpTimeInterval=" + pickUpTimeInterval +
                ", deliveryTimeInterval=" + deliveryTimeInterval + ", loadingDuration=" + loadingDuration +
                ", unloadingDuration=" + unloadingDuration + ", iconUrl=" + iconUrl + ", extras=" + extras + "]";
    }

    String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

}