package com.dealservices.rest.client;

import com.dealservices.rest.client.restattributes.Login;
import com.dealservices.rest.client.restattributes.RestAction;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deal-04 on 07/08/15.
 */
public class RestClient implements Serializable{
    final static Logger logger = Logger.getLogger(RestClient.class);
    private int httpResponse;
    private RestAction restAction;
    private String authUUID;
    private String JSESSIONID;
    CloseableHttpClient httpClient = null;
    HttpPost httpPost = null;
    CloseableHttpResponse response = null;
    HttpGet httpGet;

    public int getHttpResponse() {
        return httpResponse;
    }

    public void setHttpResponse(int httpResponse) {
        this.httpResponse = httpResponse;
    }

    public String getJSESSIONID() {
        return JSESSIONID;
    }

    public void setJSESSIONID(String JSESSIONID) {
        this.JSESSIONID = JSESSIONID;
    }

    public String getAuthUUID() {
        return authUUID;
    }

    public void setAuthUUID(String authUUID) {
        this.authUUID = authUUID;
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public HttpPost getHttpPost() {
        return httpPost;
    }

    public void setHttpPost(HttpPost httpPost) {
        this.httpPost = httpPost;
    }

    public CloseableHttpResponse getResponse() {
        return response;
    }

    public void setResponse(CloseableHttpResponse response) {
        this.response = response;
    }

    public RestAction getRestAction() {
        return restAction;
    }

    public void setRestAction(RestAction restAction) {
        this.restAction = restAction;
    }

    public void doRest(RestAction restAction){
        this.restAction=restAction; //set the local variable
        if(!(restAction.getMethod() == null ? "" == null : restAction.getMethod().equals(""))){
            if(restAction.getMethod().equals("post")){
                logger.info("Starting post method");
                doRestPost(restAction);
            }else if(restAction.getMethod().equals("get")){
                logger.info("Starting get method");
                doRestGet(restAction);
            }else if(restAction.getMethod().equals("put")){
                logger.info("Starting put method");
                doRestPut(restAction);
            }
        }else{
            //default
            doRestPost(restAction);
        }
    }
    public void doRestPost(RestAction restAction){
        try {

            httpClient = HttpClients.createDefault();
            httpPost = new HttpPost(this.restAction.getHttpUrl());

            //SET HEADER
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("Content-type", "application/json"));

            //Set the auth token and JSESSIONID to the http header
            if (!(restAction instanceof Login)) {
                nvps.add(new BasicNameValuePair("authTokens", this.getAuthUUID()));
                nvps.add(new BasicNameValuePair("JSESSIONID", this.getJSESSIONID()));
            }

            nvps.add(new BasicNameValuePair("x-kii-appid", "xxxxx"));
            nvps.add(new BasicNameValuePair("x-kii-appkey", "xxxxxxxxxxxxxx"));

            //REST INPUT || PAYLOAD BODY
            StringEntity input = new StringEntity(this.restAction.getRestInput());

            input.setContentType("application/json");
            httpPost.setEntity(input);

            for (NameValuePair h : nvps)
            {
                httpPost.addHeader(h.getName(), h.getValue());
            }

            //START Communicating with the server
            response = httpClient.execute(httpPost);
            int responseCode=response.getStatusLine().getStatusCode();
            this.httpResponse=responseCode;
            if (responseCode != 200) {
                logger.error("Failed : HTTP error code : "+response.getStatusLine().getStatusCode());
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }else {
                //Set success true
                restAction.setSuccess(true);
            }

            logger.info("Post method successfully executed");

            //keeping the server JSON response

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (response.getEntity().getContent())));

            String jsonString=restAction.outputReader(br);

            if(jsonString.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(jsonString);
                restAction.setResponseJsonArray(jsonArray);
            }else {
                restAction.setResponseJson(jsonString);
            }


            //save login session (Auth token & JSessionID) if current rest action is login

            if ((restAction instanceof Login)) {

                this.authUUID=restAction.getResponseJson().getJSONArray("accessTokenUuids").getString(0);
                String companyUUID=restAction.getResponseJson().getString("companyUuid");
                Login login = (Login) restAction;
                Header[] jsessionids = response.getHeaders("JSESSIONID");
                this.setJSESSIONID(jsessionids[0].getValue());
                login.setJSESSIONID(this.getJSESSIONID());
                login.setCompanyUUID(companyUUID);
                login.setAuthUUID(this.authUUID);
                logger.info("Login success, AuthUUid : " + authUUID+" & JSESSION_ID : "+this.getJSESSIONID() );
            }
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } finally {
            try{
                response.close();
                httpClient.close();
            }catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void doRestPut(RestAction restAction){
        try {
            HttpClient client=new HttpClient();

            PutMethod put=new PutMethod(this.restAction.getHttpUrl());

            //SET HEADER
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("Content-type", "application/json"));

            //Set the auth token and JSESSIONID to the http header
            if (!(restAction instanceof Login)) {
                nvps.add(new BasicNameValuePair("authTokens", this.getAuthUUID()));
                nvps.add(new BasicNameValuePair("JSESSIONID", this.getJSESSIONID()));
            }

            nvps.add(new BasicNameValuePair("x-kii-appid", "xxxxx"));
            nvps.add(new BasicNameValuePair("x-kii-appkey", "xxxxxxxxxxxxxx"));

            //REST INPUT || PAYLOAD BODY
            StringRequestEntity requestEntity = new StringRequestEntity( this.restAction.getRestInput(), "application/json", "UTF-8");//isaac

            put.setRequestEntity(requestEntity);

            for (NameValuePair nvp : nvps)
            {
                put.setRequestHeader(nvp.getName(), nvp.getValue());
            }

            //EXECUTE Communicating with the server
            final int intResponse = client.executeMethod(put);
            this.httpResponse=intResponse;
            InputStream responses=put.getResponseBodyAsStream();
            BufferedReader br = new BufferedReader(new InputStreamReader((responses)));

            //keeping the server JSON response
            String jsonString=restAction.outputReader(br);

            if(jsonString.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(jsonString);
                restAction.setResponseJsonArray(jsonArray);
            }else {
                if(intResponse==200){
                    JSONObject responseJson = new JSONObject(jsonString);
                    restAction.setResponseJson(responseJson);
                    logger.info("Put method Success, Response Body : " + responseJson.toString());
                }else {
                    Object responseJson = new JSONObject(jsonString).get("payload");

                    if (responseJson instanceof JSONObject) {
                        restAction.setResponseJson((JSONObject) responseJson);
                    } else if (responseJson instanceof JSONArray) {
                        restAction.setResponseJsonArray((JSONArray) responseJson);
                    }
                }
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } finally {
            try{
                response.close();
                httpClient.close();
            }catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void doRestGet(RestAction restAction){
        try {
            httpClient = HttpClients.createDefault();
            httpGet = new HttpGet(this.restAction.getHttpUrl());

            //SET HEADER
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();

            //Set the auth token and JSESSIONID to the http header
            if (!(restAction instanceof Login)) {
                nvps.add(new BasicNameValuePair("authTokens", this.getAuthUUID()));
                nvps.add(new BasicNameValuePair("JSESSIONID", this.getJSESSIONID()));
            }

            for (NameValuePair h : nvps)
            {
                httpGet.addHeader(h.getName(), h.getValue());
            }

            //START Communicating with the server
            response = httpClient.execute(httpGet);
            int responseCode=response.getStatusLine().getStatusCode();
            this.httpResponse=responseCode;
            if (responseCode != 200) {
                logger.info("Failed : HTTP error code : "+response.getStatusLine().getStatusCode());
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            //keeping the server JSON response

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (response.getEntity().getContent())));

            String jsonString=restAction.outputReader(br);

            if(jsonString.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(jsonString);
                restAction.setResponseJsonArray(jsonArray);
            }else {
                restAction.setResponseJson(jsonString);
            }

            //save login session (Auth token & JSessionID) if current rest action is login

            if ((restAction instanceof Login)) {
                this.authUUID=restAction.getResponseJson().getJSONArray("accessTokenUuids").getString(0);
                Header[] jsessionids = response.getHeaders("JSESSIONID");
                this.setJSESSIONID(jsessionids[0].getValue());
            }
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } finally {
            try{
                response.close();
                httpClient.close();
            }catch(Exception ex) {
                ex.printStackTrace();
            }
        }
        }
}
